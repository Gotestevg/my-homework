# Задача 6. Сжатие
# Вариант, который считает все уникальные значения в строке
text = [i for i in input('Введите текст: ')]
unique_elem = []
for i in text:
    if i in unique_elem:
        continue
    else:
        unique_elem.append(i)
print(unique_elem)
s = []
for i in unique_elem:
    s.append([i] + [text.count(i)])
s = [str(i) for m in s for i in m]
print(''.join(s))


# Попытка сохранить все значения в списке
text = [i for i in input('Введите текст: ')]
x = 0
count = 1
s = []
while True:
    if text[x] == text[x + 1]:
        count += 1
        x += 1
        s.append(text[x] + str(count))
    else:
        s.append(text[x] + str(count))
        break
    print(s)


# Решение из интернета: 
text = input('Введите слово: ')
count = 1
s = []

for i in range(len(text)):
    if text[i] == text[i + 1: i + 2]:
        count += 1
        continue
    s.append(text[i] + str(count))
    count = 1
print(''.join(s))