# # Задача 1. Меню ресторана
print(''.join([', ' if i == ';' else i for i in input('''Введите названия блюд через ';' ''')]))



# # Задача 2. Самое длинное слово
x = input('Введите текст: ').split()
y = 0
z = ''

for i in x:
    if len(i) > y:
        y = len(i)
        z = i
print('Самое длинное слово: ', z)
print('Длина этого слова', max([len(i) for i in x]))



# # Задача 3. Файлы
# Вариант_1
while True:   
    text = input('Введите наименование файла: ')
    if not text.endswith(('txt', 'docx')):
        print('''Расширение файла должно заканчиваться на "txt", "docx"''')
    elif text.startswith(('@', '№', '$', '%', '^', '&', '*', '(', ')')):
        print('Строка начинается с недопустимого значения')
    else:
        print('Файл назван верно')
        break

# # Вариант_2
x = list('@№$%^&*()')
text = input('Введите наименование файла: ')

x = list('@№$%^&*()')
for i in range(len(text)):
    if text.startswith(x[i]):
        print('Строка начинается с недопустимого значения')
        break
    else:
        if not text.endswith(('txt', 'docx')):
            print('''Расширение файла должно заканчиваться на "txt", "docx"''')
            break
        else:
            print('Файл назван верно')
            break
        
        
        
# # Вариант_3 (из инета)
text = input('Введите текст: ')
count = 1
s = []

for i in range(len(text)):
    if text[i] == text[i + 1: i + 2]:
        count += 1
        continue
    s.append(text[i] + str(count))
    count = 1

print(''.join(s))




# # Задача 4. Заглавные буквы
print(' '.join([i[0].upper() + i[1:] for i in input('Введите текст: ').split()]))



# # # Задача 5. Пароль
while True:
    p = input('Введите пароль: ')

    if len(p) < 8:
        print('Пароль должен состоять минимум из 8 символов\n')
    elif len([i for i in [i.isupper() for i in p] if i == True]) < 1:
        print('В пароле должна присутствовать минимум одна заглавная буква\n')
    elif len([i for i in [i.isdigit() for i in p] if i == True]) < 3:
        print('В пароле должны присутствовать минимум 3 цифры\n')
    else:
        print('Надежный пароль!')
        break




# Задача 6. Сжатие
text = input('Введите слово: ')
count = 1
s = []

for i in range(len(text)):
    if text[i] == text[i + 1: i + 2]:
        count += 1
        continue
    s.append(text[i] + str(count))
    count = 1
print(''.join(s))




# # Задача 7. IP-адрес 2
t = input('Введите ip-адрес: ')
s = []

if not ((''.join([i for i in t if i != '.'])).isdigit()):
    print('ip-адрес не может содержать буквы')
elif len([i for i in t if i == '.']) != 3:
    print('Адрес — это четыре числа, разделённые точками')
else:
    for i in t:
        if i != '.':
            s.append(i)
            if not 0 <= (int(''.join(s))) <= 255:
                print('Параметр', (int(''.join(s))), 'превышает допустимое значение')
                print('Введите значение от 0 до 255')
                break
        else:
            s = []
            continue
    else:
        print('Ваш ip-адрес:', t)



# Задача 8. Бегущая строка
s1 = input('Первая строка: ')
s2 = input('Вторая строка: ')

if s2 == s1:
    print('Строки идентичны')
elif len(s2) != len(s1):
    print('Первую строку нельзя получить из второй') 
else:
    k = 1
    mozhno = False
    for i in range(len(s2) - 1):
        s2 = s2[-1] + s2[:-1]
        if s2 == s1:
            mozhno = True
            print('Первая строка получится из второй со сдвигом', k)
            break
        else:
            k += 1
    if not mozhno:
        print('Строки нельзя сравнять')



# Задача 9. Сообщение
# (Решение из интернета)
sms = input('\nСообщение: ')

word = ''
new_sms = ''

for i in range(len(sms)):
    if ((not sms[i].isalpha() and not sms[i + 1: i + 2].isalpha()) 
        or (not sms[i].isalpha() and sms[i + 1: i + 2].isalpha())
        ):
        new_sms += sms[i]

    elif sms[i].isalpha() and sms[i + 1: i + 2].isalpha():
        word += sms[i]

    elif sms[i].isalpha() and not sms[i + 1: i + 2].isalpha():
        word += sms[i]
        new_sms += word[::-1]
        word = ''

print('Новое сообщение: ', new_sms)



# Задача 10. Истина
def decryption(messenge):
    translated = ""
    for i_word in messenge:
        if i_word in letters:
            num_index = letters.find(i_word)
            translated += letters[num_index - 1]
        else:
            translated += i_word
        return translated

def shift(text, key):
    word_ln = len(text)
    shift = key % word_ln
    text = text[-shift:] + text[:-shift]
    return text

text = 'vujgvmCfb tj ufscfu ouib z/vhm jdjuFyqm jt fscfuu uibo jdju/jnqm fTjnqm tj scfuuf ibou fy/' \
'dpnqm yDpnqmf jt cfuufs boui dbufe/dpnqmj uGmb tj fuufsc ouib oftufe/ bstfTq jt uufscf uibo otf/' \
'ef uzSfbebcjmj vout/dp djbmTqf dbtft (ubsfo djbmtqf hifopv up csfbl ifu t/svmf ipvhiBmu zqsbdujdbmju fbutc uz/' \
'qvsj Fsspst tipvme wfsof qbtt foumz/tjm omfttV mjdjumzfyq odfe/tjmf Jo fui dfgb pg hvjuz-bncj gvtfsf fui ubujpoufnq up ftt/' \
'hv Uifsf vmetip fc pof.. boe sbcmzqsfgf zpom pof pvt..pcwj xbz pu pe ju/ ' \
'Bmuipvhi uibu bzx bzn puo cf wjpvtpc bu jstug ttvomf sfzpv( i/Evud xOp tj scfuuf ibou /' \
'ofwfs uipvhiBm fsofw jt fopgu cfuufs boui iu++sjh x/op gJ ifu nfoubujpojnqmf tj eibs pu mbjo-fyq tju( b bec /' \
'jefb Jg fui foubujpojnqmfn jt fbtz up bjo-fyqm ju znb cf b hppe jefb/ ' \
'bnftqbdftO bsf pof ipoljoh sfbuh efbj .. fu(tm pe psfn gp tf"uip'.split()

letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

text_2 = []
key = 3
for i_word in text:
    text_decryption = decryption(i_word)
    shift_text = shift(text_decryption, key)
if shift_text.endswith("/"):
    key += 1
    text_2.append(shift_text)
else:
    text_2.append(shift_text)

text_2 = " ".join(text_2)
text_2 = text_2.replace("+", "*")
text_2 = text_2.replace("-", ",")
text_2 = text_2.replace("(", "'")
text_2 = text_2.replace("..", "--")
text_2 = text_2.replace('"', "!")
text_2 = text_2.replace("/", ".\n")

print(text_2)




































