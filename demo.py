# Задача 8. Считалка
# Проба 1
import itertools

count = int(input('Кол-во человек: '))
x = int(input('Какое число в считалке? '))
print('Значит, выбывает каждый', x, '- й, человек')


s = []
for i in range(1, count + 1):
    s += [i]
print('Текущий круг людей:', s)
y = int(input('Начало счета с номера: '))



g = itertools.cycle((s))
for i in range(x):
    print(next(g))
    



# Проба 2
count = int(input('Кол-во человек: '))
x = int(input('Какое число в считалке? '))
print('Значит, выбывает каждый', x, '- й, человек')



s = []

for i in range(1, count + 1):
    s.append(i)
while len(s) > 1:
    print('Текущий круг людей:', s)
    y = int(input('Начало счета с номера: '))


    f = s.index(y)
    print('Индекс числа: ', f)
    g2 = x % len(s)
    print('Разность: ', g2)
    

    print('\nВыбывает человек под номером: ', f + g2)
    del s[s.index(f + g2)]
print(s)