import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains, Keys


class Click:
    def __init__(self, xpath):
        self.xpath = xpath   
    def func_click(self):
        return browser.find_element(By.XPATH, self.xpath).click()

class Input:
    def __init__(self, xpath, text):
        self.xpath = xpath
        self.text = text
    def func_input(self):
        return browser.find_element(By.XPATH, self.xpath).send_keys(self.text)
    
# вход
url = 'https://demoqa.com/'
browser = webdriver.Chrome()
browser.get(url)
browser.maximize_window()
time.sleep(0)

# click Elements
button_1 = Click('//div[@class="card mt-4 top-card"]')
button_1.func_click()
time.sleep(0)

# click "CheckBox"
button_2 = Click('//ul[@class="menu-list"]//li[@id="item-1"]')
button_2.func_click()
time.sleep(0)

# open dir Home
button_3 = Click('//button[contains(@class,"rct-collapse")]')
button_3.func_click()
time.sleep(0)

# open under_dir_Downloads
button_4 = Click('//li[@class="rct-node rct-node-parent rct-node-expanded"]//ol/li[3]//button[@type="button"]')
button_4.func_click()
time.sleep(0)

# choose "Excel File"
button_5 = Click('//label[@for="tree-node-excelFile"]')
button_5.func_click()
time.sleep(2000)


browser.quit()










