# Задача 5. Сохранение
import os
s_path = ''
text = input('Введите строку: ')
w_path = input('Введите последовательность папок (через пробел): ').split()
file_name = input('Введите имя файла: ') + '.txt'
for i in w_path:
    s_path += i + '/'
f_path = 'C:/' + s_path
final_path = f_path + file_name

for i in os.listdir(f_path):
    t_path = os.path.abspath(os.path.join(final_path, i))
    if file_name in t_path:
        print('Такой файл уже существует в директории')
        break
    else:
        file_open = open(final_path, 'w', encoding='utf-8')
        file_open.write(text)
        print('\nФайл успешно сохранен!\nСодержимое файла: ', text)
        file_open.close()
