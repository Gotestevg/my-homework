# # # Задача 1. Ревью кода
students = {
    1: {
        'name': 'Bob',
        'surname': 'Vazovski',
        'age': 23,
        'interests': ['biology, swimming']
    },
    2: {
        'name': 'Rob',
        'surname': 'Stepanov',
        'age': 24,
        'interests': ['math', 'computer games', 'running']
    },
    3: {
        'name': 'Alexander',
        'surname': 'Krug',
        'age': 22,
        'interests': ['languages', 'health food']
    }
}


for one, two in students.items():
    print('id:', one, '-', 'age:', two['age'])
    
s = []
m = set()
g = 0

for one, two in students.items():
    s.append(two['interests'])
    g += len(two['surname'])

for i in s:
    for j in i:
        m.add(j)

print('Полный список интересов всех студентов', m)
print('Общая длина всех фамилий студентов: ', g)



# # Задача 2. Универсальная программа 2
x = list(input('Введите текст: '))
y = str(len(x))
s = []

def is_prime(a):
    if a < 2:
        return False
    for i in range(2, int(a // 2 + 1)):
        if a % i == 0:
            return False
    else:
        return True

for one, two in enumerate(x):
    if is_prime(one):
        s.append(two)
        
print(s)



# # Задача 3. Функция

x = list((input('Введите текст: ')))
y = input('Введите символ: ')
s = []

for one, two in enumerate(x):
    if two == y:
        s.append(one)

s = s[:2]

if len(s) == 1:
    print(tuple(x[s[0]:]))
elif len(s) == 2:
    print(tuple(x[s[0]:s[1] + 1]))
else:
    print(tuple())
    
    
    

# # Задача 4. Игроки

players = {
    ("Ivan", "Volkin"): (10, 5, 13),
    ("Bob", "Robbin"): (7, 5, 14),
    ("Rob", "Bobbin"): (12, 8, 2)
}
s = []

for one, two in players.items():
    s.append(tuple((one[0], one[1], two[0], two[1], two[2])))
print(s)



# # Задача 5. Одна семья
f = {
    'Петров Николай': 20,
    'Мосалев Клим': 4,
    'Салазкин Кирилл': 35,
    'Петрова Светлана': 56,
    'Складчиков Юрий': 34,
    'Салазкина Виктория': 20,
    'Салазкина Оксана': 34,
    'Складчикова Вера': 43,
    'Петрова Дарья': 18,
    'Складчикова Мария': 16,
    'Спорышев Руслан': 33,
    'Мосалев Александр': 35,
    'Спорышева Дарья': 31,
    'Мосалева Ирина': 39,
    'Спорышев Валерий': 65,
    'Складчиков Михаил': 5,
}

n = input('Введите фамилию: ').lower()
for one, two in f.items():
    if n.endswith('а'):
        n = ''.join([i for i in n][:-1])
    if n in one.lower():
        print(one, two)




# # Задача 6. По парам
import random
s = [random.randint(0, 9) for i in range(10)]
m = tuple()
g = []

for i in s:
    m += (i,)
    if len(m) == 2:
        g.append(m)
        m = ()

print('Новый список: ', g)


import random
m = tuple()
g = []

for i in range(10):
    m += (random.randint(0, 9),)
    if len(m) == 2:
        g.append(m)
        m = ()
    
print('Новый список: ', g)




# # Задача 7. Функция сортировки
import random
def func_sort(q):
    return tuple(sorted(q))

x = [random.randint(1, 20) for i in range(10)]
print(func_sort(x))



# Задача 8. Контакты 3
s = {}

while True:
    action = input('\nВведите действие:\n1 - добавить контакт \n2 - найти человека \n>>>   ')
    if action == '1':
        x = tuple((input('Введите имя и фамилию через пробел: ').split()))
        y = int(input('Введите номер телефона: '))
        if x in s:
            print('Такой контакт уже есть в словаре')
        else:
            s[x] = y 
        print('Текущий словарь контактов: ', s)

    elif action == '2':
        a = input('Введите фамилию для поиска: ')
        for one, two in s.items():
            if a.lower() == one[1].lower():
                print('Найденный телефон: {}'.format(two))
            else:
                print('Таких контактов нет')




# Задача 9. Протокол соревнований
while True:
    count = int(input('Кол-во записей: '))
    if count < 3:
        print('\nМинимальное кол-во записей: 3')
    else:
        break
    
s = {}
s_final = {}
k = 1

for i in range(1, count + 1):
    print('{}-я запись:'.format(i))
    r = int(input('Введите результат: '))
    n = input('Введите имя: ')
    s[r] = n
    

m = sorted(s.keys())[-3:][::-1]
    
for i in m:
    for one, two in s.items():
        if i == one:
             s_final[two] = one
            
for one, two in s_final.items():
    print('{}-е место.'.format(k), one, two)
    k += 1





# # Задача 10. Своя функция zip
a = 'abc'
b = (10, 20, 30, 40)

def func0(one, two):
    return min(len(one), len(two))

final = [(a[i], b[i]) for i in range(func0(a, b))]
for i in final:
    print(i)
print(zip(a, b))




















































