# Задача 1. Драка
import random

class warrior:
    def __init__(self, name, health=100):
        self.name=name
        self.health = health
    def func_hit(self, damage):
            damage.health -= 20


warriors = [warrior('Ахилес'), warrior('Гектор')]

while True:
    move = int(input('Выберите действие: \n1 - атака, 2 - выход\n >> '))
    if move == 1:
        x = random.randint(0, 1)
        attacker, victim = warriors[x], warriors[x-1]
        attacker.func_hit(victim)
        print('{} нанес удар по {}'.format(
            attacker.name, victim.name
        ))
        print('У {} осталось {} здоровья'.format(
            victim.name, victim.health
        ))
        if victim.health <= 0:
            print('Воин по имени {} выирал эту битву'.format(
                attacker.name
            ))
            break
    else:
        print('Выход из игры')
        break



# Задача_2. студенты
import random
from russian_names import RussianNames
class Student:
    def __init__(self, name, score, group_number, group_name, faculty):
        self.faculty = faculty
        self.group_number = group_number
        self.group_name = group_name
        self.name = name
        self.score = score
    def func_print_info(self):
        print('Факультет: {}\nНомер группы: {}\nИмя группы: {}\nСтудент: {}\nУспеваемость: {}\n'.format(
            self.faculty, self.group_number, self.group_name, self.name, self.score
        ))

group_names = ('Green', 'Red', 'Blue', 'Navy', 'White', 'Black', 'Orange', 'Broun')
facultys = ('Экономический', 'Юридический', 'ИБ', 'Исторический', 'Филологический', 'Наук')

how_mutch_students = int(input('Кол-во студентов: '))
for i in range(1, how_mutch_students + 1):
    print('\n{}-й студент:'.format(i))
    student = Student(
        group_name=random.choice(group_names),
        group_number=random.randint(1, 5),
        name=RussianNames().get_person(),
        faculty=random.choice(facultys),
        score=random.randint(1, 10)
    )

    student.func_print_info()



# Задача 3. Круг
from math import pi


class Round:
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r
        

    def func_print_square(self):
        print('\nX: {}\nY: {}\nR: {}\n'.format(
            self.x, self.y, self.r
        ))

    def func_get_square(self):
        s = round(pi * r ** 2, 1)
        print('Площадь круга: {}\n'.format(
            s
        ))

    def func_get_per(self):
        p = round(r * 2 * pi, 1)
        print('Длина окружности: {}\n'.format(
            p
        ))
    def func_volume_size(self, v):
        
        x = x * v
        y = y * v
        r = r * v
        print('\nX: {}\nY: {}\nR: {}\n'.format(
            self.x, self.y, self.r
        ))


while True:
    x, y, r = int(input('ввод X: ')), int(input('ввод Y: ')), int(input('ввод R: '))
    
    q = int(input('Действие: 1-площадь, 2-длина, 3-пересечение\n >>>  '))
    round_1 = Round(x, y, r)
    if q == 1:
        round_1.func_get_square()
    if q == 2:
        round_1.func_get_per()
    if q == 3:
        round_1.func_volume_size()
        
        


# Задача 4. Отцы, матери и дети
import random
from class_parents import parents
class parents:
    def __init__(self, name, age, child_list):
        self.name = name
        self.age = age
        self.child_list = child_list

    def func_print_info(self):
        print('Меня зовут {}\nМне {} лет\nУ меня есть дети {}\n'.format(
            self.name, self.age, self.child_list
        ))

    def func_feed_the_baby(self):
        foods = ['potato&meet', 'rice&fish', 'icecream', 'cola', 'berries', 'apple']
        print('Ребенок по имени {} получил {}\n'.format(
            random.choice(self.child_list),
            random.choice(foods)
        ))

    def func_keep_calm(self):
        k = ['Ребенок успокоился', 'Ребенок не успокоился']
        print('Попытка успокоить ребенка: {}\n'.format(random.choice(k)))

class child:
    def __init__(self, name, age, calm, hungry):
        self.name = name
        self.age = age
        self.calm = calm
        self.hungry = hungry
    def func_print_info(self):
        print('Меня зовут {}\nмне {} лет\nмое состояние: {}\nголод: {}\n'.format(
            self.name, self.age, self.calm, self.hungry
        ))

f = ['Max', 'Chloe']
parent_1 = parents('John', 36, f)
child_1 = child('Max', 15, 'спокоен', 'не голоден')
child_2 = child('Chloe', 10, 'не спокоен', 'голоден')

while True:
    move = int(input('Действие родителя:\n1 - инфо о себе\n2 - покормить ребенка\n3 - успокоить ребенка\n0 - прервать программу\n >>>  '))
    if move == 1:
        parent_1.func_print_info()
    if move == 2:
        parent_1.func_feed_the_baby()
    if move == 3:
        parent_1.func_keep_calm()
    if move == 0:
        print('Выход из программы...')
        break

    move_c = int(input('1 - Информация о ребенке\n0 - прервать программу\n >>>  '))
    if move == 1:
        child_1.func_print_info()
        child_2.func_print_info()
    if move == 2:
        print('Выход из программы...')
        break



# Задача_5. Веселая ферма 2
class Potato:
    states = {0: 'Отсутствует', 1: 'Росток', 2: 'Зеленая', 3: 'Зрелая'}

    def __init__(self, index):
        self.index = index
        self.state = 0

    def grow(self):
        if self.state < 3:
            self.state += 1
        self.print_state()

    def is_ripe(self):
        if self.state == 3:
            return True
        return False

    def print_state(self):
        print(f'Картошка {self.index} сейчас {Potato.states[self.state]}')


class PotatoGarden:

    def __init__(self, count):
        self.potatoes = [Potato(index) for index in range(1, count + 1)]

    def grow_all(self):
        print('Картошка прорастает!')
        for potato in self.potatoes:
            potato.grow()

    def are_all_ripe(self):
        for potato in self.potatoes:
            if not potato.is_ripe():
                return False
        else:
            return True

    def print_all_states(self):
        for potato in self.potatoes:
            potato.print_state()


class Gardener:

    def __init__(self, name, pg):
        self.name = name
        self.pg = pg
        self.collect = 0

    def hoeing(self):
        print(f'Садовник {self.name} окучивает картошку')
        self.pg.grow_all()

    def check(self):
        if self.pg.are_all_ripe():
            return True
        return False

    def to_collect(self):
        print(f'Садовник {self.name} собирает картошку')
        self.collect += len(self.pg.potatoes)
        self.pg.potatoes = []

    def act(self):
        if self.check():
            self.to_collect()
            print(f'Садовник {self.name} собрал {self.collect} картошки')
            return True
        else:
            self.hoeing()
            return False


gardener = Gardener('Гоша', PotatoGarden(5))

while True:
    if gardener.act():
        break




# Задача 6. Магия
class sostoyaniya:
    name_1 = 'Пар'
    name_2 = 'Шторм'
    name_3 = 'Грязь'
    name_4 = 'Молния'
    name_5 = 'Пыль'
    name_6 = 'Лава'

class elements:
    def func_fire(self):
        return 'огонь'
    def func_water(self):
        return 'вода'
    def func_air(self):
        return 'воздух'
    def func_earth(self):
        return 'земля'


a = elements().func_fire()   # огонь
b = elements().func_water()  # вода
c = elements().func_air()    # воздух
d = elements().func_earth()  # земля


while True:
    fir_elem = input('\n1-й элемент: ')
    sec_elem = input('2-й элемент: ')

    if fir_elem + sec_elem == 'ab' or fir_elem + sec_elem == 'ba':
        print(sostoyaniya.name_1)
    elif fir_elem + sec_elem == 'bc' or fir_elem + sec_elem == 'cb':
        print(sostoyaniya.name_2)
    elif fir_elem + sec_elem == 'bd' or fir_elem + sec_elem == 'db':
        print(sostoyaniya.name_3)
    elif fir_elem + sec_elem == 'ca' or fir_elem + sec_elem == 'ac':
        print(sostoyaniya.name_4)
    elif fir_elem + sec_elem == 'cd' or fir_elem + sec_elem == 'dc':
        print(sostoyaniya.name_5)
    elif fir_elem + sec_elem == 'ad' or fir_elem + sec_elem == 'da':
        print(sostoyaniya.name_6)




# Задача 7. Совместное проживание
import random
########################################################################################################################
class Human:

    def __init__(self, name, degree_satiety=50):
        self.name = name
        self.degree_satiety = degree_satiety

    def eat(self):
        print(f'{self.name}! Вы проголодались и решили перекусить.')
        he_human.degree_satiety += 1
        print(f'Степень сытости: {he_human.degree_satiety}')
        House.refrigerator_with_food -= 2
        print(f'Холодильник с едой: {House.refrigerator_with_food}')

    def work(self):
        print(f'{self.name}! Вы пошли на работу...')
        self.degree_satiety -= 7
        print(f'Степень сытости: {self.degree_satiety} {self.name}')
        House.nightstand_with_money += 5
        print(f'Tумбочка с деньгами {House.nightstand_with_money}')

    def play(self):
        self.degree_satiety -= 5
        print(f'{self.name}! Вы пошли играть!\nСтепень сытости: {self.degree_satiety}')

    def go_shopping(self):
        print(f'{self.name}! Вы пошли за покупками')
        House.refrigerator_with_food += 10
        print(f'Холодильник с едой: {House.refrigerator_with_food}')
        House.nightstand_with_money -= 5
        print(f'Tумбочка с деньгами {House.nightstand_with_money}')


    def logic_human_actions(self):
        count = 0
        while he_human.degree_satiety > 0 and she_human.degree_satiety > 0:
            print(f'\nДень {count}:')
            print()
            dice_number = random.randint(1, 6)
            if he_human.degree_satiety < 20 or she_human.degree_satiety < 20:
                self.eat()
            elif House.refrigerator_with_food < 10:
                self.go_shopping()
            elif House.nightstand_with_money < 50:
                self.work()
            elif dice_number == 1:
                self.work()
            elif dice_number == 2:
                self.eat()
            else:
                self.play()
            count += 1
            if count == 365:
                print(f'{self.name} остался жив! Его сытость равна {self.degree_satiety}'
                      f'\nОстаток денег: {House.nightstand_with_money}')
                break
        else:
            print('ВЫ ПoМЕРЛИ')
########################################################################################################################
class House:

    refrigerator_with_food = 50
    nightstand_with_money = 0

########################################################################################################################
he_human = Human('Вадик')
she_human = Human('Зоя')

he_human.logic_human_actions()
she_human.logic_human_actions()




# Задача 8. Блек-джек
import random
class card:
    cards = [1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,11,11]

class desk:
    def func_random():
        main_card = random.choice(card.cards)
        card.cards.remove(main_card)
        return main_card
        
class player:
    def __init__(self,name, summa):
        self.name = name
        self.summa = 0

player_1 = player('Кирилл', 36)
player_2 = player('Компуктер', 100)

desk.func_random()


for i in range(2):
    player_1.summa += desk.func_random()
    player_2.summa += desk.func_random()

def get_one_card():
    player_1.summa += desk.func_random()
    player_2.summa += desk.func_random()

print('Ваша рука: {}'.format(player_1.summa))
if player_1.summa > 21 or player_2.summa > 21:
    print('пересдача')
else:
    while True:
        move = int(input('Еще карту? \n1 - да\n2 - нет\n >>> '))
        if move == 1:
            get_one_card()
            if player_1.summa > 21 and player_2.summa <= 21:
                print('{} Перебор. Вы проиграли'.format(player_1.summa))
                print('Крупье набрал {}'.format(player_2.summa))
                break
            elif player_1.summa <= 21 and player_2.summa > 21:
                print('Вы победили, набрав {}'.format(player_1.summa))
                print('Крупье набрал {}'.format(player_2.summa))
                break
            elif player_1.summa > 21 and player_2.summa > 21:
                print('Вы и крупье проиграли, набрав по {}. Ничья. Все остаются при своих'.format(player_1.summa))
                break
        
        elif move == 2:
            if player_1.summa > player_2.summa:
                print('Вы победили, набрав {}'.format(player_1.summa))
                print('Крупье набрал {}'.format(player_2.summa))
            elif player_1.summa < player_2.summa:
                print('Вы проиграли, набрав {}'.format(player_1.summa))
                print('Крупье набрал {}'.format(player_2.summa))
            else:
                print('Вы и крупье проиграли, набрав по {}. Ничья. Все остаются при своих'.format(player_1.summa))
            break




# Задача 9. Крестики-нолики
STEP_DICT = {1: '00', 2: '01', 3: '02', 4: '10', 5: '11', 6: '12', 7: '20', 8: '21', 9: '22'}
WIN_LIST = (('00', '01', '02'), ('10', '11', '12'), ('20', '21', '22'), ('00', '10', '20'),
            ('01', '11', '22'), ('02', '12', '22'), ('00', '11', '22'), ('02', '11', '20'))
MATRIX = [[1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]]


def print_matrix():
    print('Крестики-нолики:')
    for elem in MATRIX:
        print(*elem)


def step_matrix(step, number):
    if step in STEP_DICT.keys():
        line = int(STEP_DICT[step][0])
        column = int(STEP_DICT[step][1])
        if number == 0:
            MATRIX[line][column] = 'X'
            STEP_DICT.pop(step)
        else:
            MATRIX[line][column] = '0'
            STEP_DICT.pop(step)
        return True
    else:
        return False


def check_win():
    for win in WIN_LIST:
        if MATRIX[int(win[0][0])][int(win[0][1])] == MATRIX[int(win[1][0])][int(win[1][1])] == \
                MATRIX[int(win[2][0])][int(win[2][1])]:
            return True
    else:
        return False


player1 = input('Введите имя первого игрока: ')
player2 = input('Введите имя второго игрока: ')

print(f'\n{player1}, Вы играете крестиками!\n'
      f'{player2}, Вы играете ноликами!\n')

players = [player1, player2]
print_matrix()
N = 2
game = True
while game:
    for i_step in range(N):
        if STEP_DICT.keys():
            while True:
                step = input(f'\n{players[i_step]} Ваш ход: ')
                if step.isdigit() and 0 < int(step) < 10:
                    if step_matrix(int(step), i_step):
                        break
                    else:
                        print('Клетка занята, попробуйте еще раз!')
                        print_matrix()
                else:
                    print('Вы сделали неверный шаг! '
                          'Такой клетки не существует!')
            print_matrix()
            if check_win():
                print(f'Поздравляем {players[i_step]}! Вы выиграли!')
                game = False
                break
        else:
            print('Ничья!')
            game = False
            break


























































