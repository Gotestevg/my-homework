# # # Задача 1. Challenge 2
def func(n, a = 1):
    print(a)
    if a != n:
        func(n, a + 1)

x = int(input('Введите число: '))
func(x)



# # Задача 2. Свой zip 2
# не понял условие задачи, вроде так
def func(a, b):
    print(list(zip(a, b)))
x = [1, 2, 3, 4, 5, 6]
y = {'a', 'b', 'c', 'd', 'e', 'f'}
func(x, y)



# Задача 3. Ряд Фибоначчи
def fibonacci(n):
    if n <= 2:
        return 1
    return fibonacci(n-1) + fibonacci(n-2)

x = int(input('Введите число: '))
print('Число Фибоначчи на', x, 'м месте:', fibonacci(x))



# Задача 4. Поиск элемента 2
site_dict = {
    "html": {
        "head": {"title": "Мой сайт"},
        "body": {
            "h2": "Здесь будет мой заголовок",
            "div": "Тут, наверное, какой-то блок",
            "p": "А вот здесь новый абзац",
        },
    }
}

def search_element(element, level=0, site=site_dict):
    if isinstance(site, dict):
        for key, value in site.items():
            if key == element:
                return value
            else:
                if isinstance(value, dict):
                    return search_element(element, level + 1, value)
                else:
                    return None

print(
    search_element(
        input("Введите искомый ключ: "),
        0
        if input("Хотите ввести максимальную глубину? ").lower() == "n"
        else int(input("Введите максимальную глубину: ")),
    )
)



# Задача 5. Ускоряем работу функции
def calculating_math_func_2(data, cache={}):
    cache = cache if isinstance(cache, dict) else dict()
    if data in cache:
        return cache[data]
    result = 1
    for index in range(1, data + 1):
        result *= index
    result /= data ** 3
    result = result ** 10
    cache[data] = result
    return cache[data]



# Задача 6. Глубокое копирование
import copy

site = {
    'html': {
        'head': {
            'title': 'Куплю/продам телефон недорого'
        },
        'body': {
            'h2': 'У нас самая низкая цена на iPhone',
            'div': 'Купить',
            'p': 'Продать'
        }
    }
}


def change_value(struct, key, value):
    if key in struct:
        struct[key] = value
    else:
        for sub_struct in struct.values():
            if isinstance(sub_struct, dict):
                change_value(sub_struct, key, value)

    return struct


def display_struct(struct, spaces=1):
    for key, value in struct.items():
        if isinstance(value, dict):
            print(' ' * spaces, key)
            display_struct(value, spaces + 3)
        else:
            print("{}{} : {}".format(' ' * spaces, key, value))


def make_site(name):
    struct_site = copy.deepcopy(site)
    new_title = 'Куплю/продам {} недорого'.format(name)
    struct_site = change_value(struct_site, 'title', new_title)
    new_h2 = 'У нас самая низкая цена на {}'.format(name)
    struct_site = change_value(struct_site, 'h2', new_h2)

    return struct_site


sites = []
sites_count = int(input('Сколько сайтов: '))
for _ in range(sites_count):
    product_name = input('Введите название продукта для нового сайта:')
    new_site = make_site(product_name)
    sites.append(new_site)
    for i_site in sites:
        display_struct(i_site)



# Задача 7. Продвинутая функция sum
def func(n):
    s = 0
    for i in n:
        if not isinstance(i, list):
            s += i
        else:
            s += func(i)
    return s

x = ([[1, 2, [3]], [1], 3])
print(func(x))



# Задача 8. Список списков 2
def func(n):
    for i in n:
        if isinstance(i, list):
            for j in func(i):
                yield j
        else:
            yield i

x = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]], [[11, 12, 13], [14, 15], [16, 17, 18, [19, [20, [21]]]]]]
print(list(func(x)))



# Задача 9. Ханойские башни
def move(n, start, finish):
    if n > 0:
        temp = 6 - start - finish
        move(n - 1, start, temp)
        print("Перенести диск", n, "со стержня", start, "на стержень", finish)
        move(n - 1, temp, finish)





















































































































