import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains, Keys



# from selenium.webdriver.chrome.options import Options

bug_number = int(input('Ввести номер бага: '))
main_folder = int(input('\nВыбрать папку:\nUbuntu Firefox - 1\nАвторизация - 2\nВерсия для печати - 3\nВерстка - 4\nЖурнал - 5\nИзменения NGFW-6\nМЕНЮ-7\nНастройки-8\nОбъекты - 9\nОтчеты-10\nПолитики - 11\nШлюзы - 12\n >>>  '))


# Ubuntu firefox
if main_folder == 1:
    under_folder = int(input('\nВыбрать подпапку: \nОбщее - 1\nВерстка - 2\n >>>  '))
    if under_folder == 1:
        main_folder = '"Ubuntu Firefox"'
        add_case = 63868
    elif under_folder == 2:
        main_folder = '"Ubuntu Firefox"'
        add_case = 67161
        
# авторизация
elif main_folder == 2:
    under_folder = int(input('\nВыбрать подпапку: \nАвторизация - 1\ncookies - 2\nВерстка - 3\n >>>  '))
    if under_folder == 1:
        main_folder = '"Авторизация"'
        add_case = 67156
    elif under_folder == 2:
        main_folder = '"Авторизация"'
        add_case = 67126
    elif under_folder == 3:
        main_folder = '"Авторизация"'
        add_case = 67347

# Версия для печати
elif main_folder == 3:
    under_folder = int(input('\nВыбрать подпапку: \nПолитики - 1\nОбъекты - 2\nОбщие элементы - 3\nОтчеты - 4\nЖурнал - 5\nНастройки - 6\nШлюзы и Gateway - 7\nИзменения - 8\n >>>  '))
    
    if under_folder == 1:
        under_under_folder = int(input('\nВыбрать папку в Политики: \nОстаться в Политики - 0\nМодальные окна - 1\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Версия для печати"'
            add_case = 63611
        elif under_under_folder == 1:
            main_folder = '"Версия для печати"'
            add_case = 63610
            
    elif under_folder == 2:
        under_under_folder = int(input('\nВыбрать папку в Объекты: \nОстаться в Объекты - 0\nМодальные окна - 1\nПравый фрейм - 2\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Версия для печати"'
            add_case = 63612
        elif under_under_folder == 1:
            main_folder = '"Версия для печати"'
            add_case = 63614
        elif under_under_folder == 2:
            main_folder = '"Версия для печати"'
            add_case = 66883
            
    elif under_folder == 3:
        main_folder = '"Версия для печати"'
        add_case = 63615
    elif under_folder == 4:
        main_folder = '"Версия для печати"'
        add_case = 64591
    elif under_folder == 5:
        main_folder = '"Версия для печати"'
        add_case = 64592
    elif under_folder == 6:
        main_folder = '"Версия для печати"'
        add_case = 66824
    elif under_folder == 7:
        main_folder = '"Версия для печати"'
        add_case = 66832
    elif under_folder == 8:
        main_folder = '"Версия для печати"'
        add_case = 66847
        
# Верстка
elif main_folder == 4:
    under_folder = int(input('\nВыбрать подпапку: \nОстаться в Объекты - 0\nЭлементы - 1\nHeader - 2\nПоле редактирования - 3\nТаблица - 4\nПоля - 5\nВсплывающие окна - 6\n >>>  '))
    if under_folder == 0:
        main_folder = '"Верстка"'
        add_case = 63287
    elif under_folder == 1:
        under_under_folder = int(input('\nВыбрать папку в Элементы: \nОстаться в Элементы - 0\nШрифт - 1\nBackground-Color - 2\nПоля - 3\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Верстка"'
            add_case = 63288
        elif under_under_folder == 1:
            main_folder = '"Верстка"'
            add_case = 63290
        elif under_under_folder == 2:
            main_folder = '"Верстка"'
            add_case = 63291
        elif under_under_folder == 3:
            main_folder = '"Верстка"'
            add_case = 63292
    elif under_folder == 2:
        under_under_folder = int(input('\nВыбрать папку в Header: \nОстаться в Header - 0\nЭлементы - 1\nBackground-Color - 2\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Верстка"'
            add_case = 63293
        elif under_under_folder == 1:
            under_under_under_folder = int(input('\nВыбрать папку в Элементы: \nОстаться в Элементы - 0\nМод. окно "Добавить объект" (через кнопку "+" в Header) - 1\n >>>  '))
            if under_under_under_folder == 0:
                main_folder = '"Верстка"'
                add_case = 63294
            elif under_under_under_folder == 1:
                main_folder = '"Верстка"'
                add_case = 63970
        elif under_under_folder == 2:
            main_folder = '"Верстка"'
            add_case = 63295
    elif under_folder == 3:
        under_under_folder = int(input('\nВыбрать папку в Поле редактирования: \nОстаться в Поле редактирования - 0\nОбщие элементы - 1\nУникальные элементы - 2\nШрифт - 3\nBackground-color - 4\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Верстка"'
            add_case = 63296
        elif under_under_folder == 1:
            main_folder = '"Верстка"'
            add_case = 63297
        elif under_under_folder == 2:
            under_under_under_folder = int(input('\nВыбрать папку в Уникальные элементы: \nОстаться в Уникальные элементы - 0\nОкно для добавления файла (IP-адреса) - 1\n >>>  '))
            if under_under_under_folder == 0:
                main_folder = '"Верстка"'
                add_case = 63298
            elif under_under_under_folder == 1:
                main_folder = '"Верстка"'
                add_case = 63299
        elif under_under_folder == 3: #шрифт
            main_folder = '"Верстка"'
            add_case = 63300
        elif under_under_folder == 4: #Background-color
            main_folder = '"Верстка"'
            add_case = 63301
    elif under_folder == 4: #таблица
        under_under_folder = int(input('\nВыбрать папку в Таблица: \nОстаться в Таблица - 0\nИконки - 1\nBackground-Color - 2\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Верстка"'
            add_case = 63302
        elif under_under_folder == 1:
            main_folder = '"Верстка"'
            add_case = 63305
        elif under_under_folder == 2:
            main_folder = '"Верстка"'
            add_case = 63306
    elif under_folder == 5: #поля
        main_folder = '"Верстка"'
        add_case = 63303
    elif under_folder == 6: #всплывающие окна
        main_folder = '"Верстка"'
        add_case = 63304
            


# Журнал
elif main_folder == 5:
    under_folder = int(input('\nВыбрать подпапку: \nФункциональность - 1\nВерстка - 2\nМодальные окна - 3\n >>>  '))
    
    if under_folder == 1:
        under_under_folder = int(input('\nВыбрать папку в Функциональность: \nОстаться в Функциональность - 0\nHeader - 1\nLeft - 2\nRight - 3\nТаблица - 4\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Журнал"'
            add_case = 64562
        elif under_under_folder == 1:
            main_folder = '"Журнал"'
            add_case = 64564
        elif under_under_folder == 2:
            main_folder = '"Журнал"'
            add_case = 64565
        elif under_under_folder == 3:
            main_folder = '"Журнал"'
            add_case = 64566
        elif under_under_folder == 4:
            main_folder = '"Журнал"'
            add_case = 64567
            
    elif under_folder == 2:
        under_under_folder = int(input('\nВыбрать папку в Верстка: \nОстаться в Верстка - 0\nHeader - 1\nLeft - 2\nRight - 3\nТаблица - 4\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Журнал"'
            add_case = 64563
        elif under_under_folder == 1:
            main_folder = '"Журнал"'
            add_case = 64568
        elif under_under_folder == 2:
            main_folder = '"Журнал"'
            add_case = 64569
        elif under_under_folder == 3:
            main_folder = '"Журнал"'
            add_case = 64570
        elif under_under_folder == 4:
            main_folder = '"Журнал"'
            add_case = 64571
            
    elif under_folder == 3:
        main_folder = '"Журнал"'
        add_case = 64572
        
        
# Изменения NGFW
elif main_folder == 6:
    under_folder = int(input('\nВыбрать подпапку: \nФункциональность - 1\nFigma - 2\n >>>  '))
    if under_folder == 1:
        under_under_folder = int(input('\nВыбрать папку в Функциональность: \nОстаться в Функциональность - 0\nКнопки - 1\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Изменения NGFW"'
            add_case = 63311
        elif under_under_folder == 1:
            main_folder = '"Изменения NGFW"'
            add_case = 63314
    elif under_folder == 2:
        main_folder = '"Изменения NGFW"'
        add_case = 63312


# Меню
elif main_folder == 7:
    under_folder = int(input('\nВыбрать подпапку: \nМеню - 1\n >>>  '))
    main_folder = '"МЕНЮ"'
    add_case = 63879
    
    

# Настройки
elif main_folder == 8:
    under_folder = int(input('\nВыбрать подпапку: \nФункциональность - 1\nПортал - 2\nМодальное окно "Добавить объект" - 3\nВерстка - 4\n >>>  '))
    
    if under_folder == 1:
        under_under_folder = int(input('\nВыбрать папку в Функциональность: \nОстаться в Функциональность - 0\nHeader - 1\nLeft - 2\nRight - 3\nТаблица - 4\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Настройки"'
            add_case = 63991
        elif under_under_folder == 1:
            main_folder = '"Настройки"'
            add_case = 64496
        elif under_under_folder == 2:
            main_folder = '"Настройки"'
            add_case = 64497
        elif under_under_folder == 3:
            main_folder = '"Настройки"'
            add_case = 64498
        elif under_under_folder == 4:
            main_folder = '"Настройки"'
            add_case = 64499
    elif under_folder == 2:
        under_under_folder = int(input('\nВыбрать папку в Портал: \nОстаться в Портал - 0\nФункциональность - 1\nВерстка\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Настройки"'
            add_case = 67349
        elif under_under_folder == 1:
            main_folder = '"Настройки"'
            add_case = 67350
        elif under_under_folder == 2:
            main_folder = '"Настройки"'
            add_case = 67351
    elif under_folder == 3:
        under_under_folder = int(input('\nВыбрать папку в Портал: \nОстаться в Модальное окно "Добавить объект" - 0\nФункциональность - 1\nВерстка\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Настройки"'
            add_case = 64552
        elif under_under_folder == 1:
            main_folder = '"Настройки"'
            add_case = 64554
        elif under_under_folder == 2:
            main_folder = '"Настройки"'
            add_case = 64553
    elif under_folder == 4:
        under_under_folder = int(input('\nВыбрать папку в Верстка: \nОстаться в Верстка - 0\nHeader - 1\nLeft - 2\nRight - 3\nТаблица - 4\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Настройки"'
            add_case = 63992
        elif under_under_folder == 1:
            main_folder = '"Настройки"'
            add_case = 64504
        elif under_under_folder == 2:
            main_folder = '"Настройки"'
            add_case = 64505
        elif under_under_folder == 3:
            main_folder = '"Настройки"'
            add_case = 64506
        elif under_under_folder == 4:
            main_folder = '"Настройки"'
            add_case = 64507
    
    # объекты
elif main_folder == 9:
    under_folder = int(input('\nВыбрать подпапку:\nОстаться в Объекты - 0\nИнтерфейс - 1\nОбъекты - 2\nПрочие действия - 3\n >>>  '))
    if under_folder == 0:
        main_folder = '"Объекты"'
        add_case = 63119
    elif under_folder == 1:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Интерфейс - 0\nФильтры - 1\nHeader - 2\nОбщие кнопки - 3\nОбщие поля внутри объектов - 4\n >>>  '))
        # Интерфейс
        if under_under_folder == 0:
            main_folder = '"Объекты"'
            add_case = 63139
        elif under_under_folder == 1:
            main_folder = '"Объекты"'
            add_case = 63122
        elif under_under_folder == 2:
            main_folder = '"Объекты"'
            add_case = 63145
        elif under_under_folder == 3:
            main_folder = '"Объекты"'
            add_case = 63157
        elif under_under_folder == 4:
            main_folder = '"Объекты"'
            add_case = 63158
            # Объекты
    elif under_folder == 2:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Объекты - 0\nВеб-сайты URL - 1\nГеография - 2\nПараметры авторизации - 3\nПриложения - 4\nПротоколы - 5\nРасписание - 6\nСотрудники - 7\nТеги - 8\nIP-адреса - 9\nMime-типы - 10\nДействия - 11\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Объекты"'
            add_case = 63159
        elif under_under_folder == 1:
            main_folder = '"Объекты"'
            add_case = 63113
        elif under_under_folder == 2:
            main_folder = '"Объекты"'
            add_case = 63172
        elif under_under_folder == 3:
            main_folder = '"Объекты"'
            add_case = 63169
        elif under_under_folder == 4:
            main_folder = '"Объекты"'
            add_case = 63112        
        elif under_under_folder == 5:
            main_folder = '"Объекты"'
            add_case = 63174 
        elif under_under_folder == 6:
            main_folder = '"Объекты"'
            add_case = 63171 
        elif under_under_folder == 7:
            under_under_under_folder = int(input('\nВыбрать подпапку:\nСотрудники - 1\nПодразделения - 2\n >>>  '))
            if under_under_under_folder == 1:
                main_folder = '"Объекты"'
                add_case = 63103
            elif under_under_under_folder == 2:
                main_folder = '"Объекты"'
                add_case = 63116
        elif under_under_folder == 8:
            main_folder = '"Объекты"'
            add_case = 63167 
        elif under_under_folder == 9:
            main_folder = '"Объекты"'
            add_case = 63168
        elif under_under_folder == 10:
            main_folder = '"Объекты"'
            add_case = 63173
            # Действия
        elif under_under_folder == 11:
            under_under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Действия - 0\nАнтивирус - 1\nЗащита контента - 2\nЗащита от DDoS - 3\nЖурналирование в SYSLOG - 4\nЖурналирование в базу данных - 5\nПроверка IPS/IDS - 6\nСканер уязвимостей - 7\nСоздание инцидента - 8\nСтраница блокировки - 9\nУведомление в Telegram - 10\nУведомление по e-mail - 11\nУдаление содержимого - 12\nПриоритет обслуживания - 13\nAdBlock - 14\nICAP - 15\nDNAT - 16\nSNAT - 17\nSSL-инспекция - 18\nSSL-Верификация - 19\nE-mail инспекция - 20\nWeb application firewall - 21\n >>>  '))
            if under_under_under_folder == 0:
                main_folder = '"Объекты"'
                add_case = 63175
            elif under_under_under_folder == 1:
                main_folder = '"Объекты"'
                add_case = 63170
            elif under_under_under_folder == 2:
                main_folder = '"Объекты"'
                add_case = 63164
            elif under_under_under_folder == 3:
                main_folder = '"Объекты"'
                add_case = 63178
            elif under_under_under_folder == 4:
                main_folder = '"Объекты"'
                add_case = 63183       
            elif under_under_under_folder == 5:
                main_folder = '"Объекты"'
                add_case = 63182 
            elif under_under_under_folder == 6:
                main_folder = '"Объекты"'
                add_case = 63179 
            elif under_under_under_folder == 7:
                main_folder = '"Объекты"'
                add_case = 64535
            elif under_under_under_folder == 8:
                main_folder = '"Объекты"'
                add_case = 63163
            elif under_under_under_folder == 9:
                main_folder = '"Объекты"'
                add_case = 63184 
            elif under_under_under_folder == 10:
                main_folder = '"Объекты"'
                add_case = 63284 
            elif under_under_under_folder == 11:
                main_folder = '"Объекты"'
                add_case = 63282
            elif under_under_under_folder == 12:
                main_folder = '"Объекты"'
                add_case = 64536
            elif under_under_under_folder == 13:
                main_folder = '"Объекты"'
                add_case = 67336 
            elif under_under_under_folder == 14:
                main_folder = '"Объекты"'
                add_case = 63177 
            elif under_under_under_folder == 15:
                main_folder = '"Объекты"'
                add_case = 63176 
            elif under_under_under_folder == 16:
                main_folder = '"Объекты"'
                add_case = 63165 
            elif under_under_under_folder == 17:
                main_folder = '"Объекты"'
                add_case = 63166 
            elif under_under_under_folder == 18:
                main_folder = '"Объекты"'
                add_case = 63181 
            elif under_under_under_folder == 19:
                main_folder = '"Объекты"'
                add_case = 63180
            elif under_under_under_folder == 20:
                main_folder = '"Объекты"'
                add_case = 67121
            elif under_under_under_folder == 21:
                main_folder = '"Объекты"'
                add_case = 63283
        
    elif under_folder == 3:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Прочие действия - 0\nОбщие действия - 1\nВалидность полей - 2 >>>  '))
        if under_under_folder == 0:
            main_folder = '"Объекты"'
            add_case = 63160
        elif under_under_folder == 1:
            main_folder = '"Объекты"'
            add_case = 63162
        elif under_under_folder == 2:
            main_folder = '"Объекты"'
            add_case = 63140

# Политики
elif main_folder == 11:
    under_folder = int(input('\nВыбрать подпапку:\nОстаться в Политики - 0\nИнтерфейс - 1\nТаблица(Выбор политики) - 2\nОсновная таблица - 3\nПравила - 4\nСлои - 5\nВерстка - 6\n >>>  '))
    if under_folder == 0:
        main_folder = '"Политики"'
        add_case = 63242
    #Интерфейс
    elif under_folder == 1:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Интерфейс - 0\nHeader - 1\nОбщие кнопки - 2\nКарандаш редактирования и добавления условия - 3\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Политики"'
            add_case = 63250
        elif under_under_folder == 1:
            main_folder = '"Политики"'
            add_case = 63243
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63251
        elif under_under_folder == 3:
            main_folder = '"Политики"'
            add_case = 63975
    #Таблица (Выбор политики)
    elif under_folder == 2:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Таблица (Выбор политики) - 0\nФильтры - 1\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Политики"'
            add_case = 63261
        elif under_under_folder == 1:
            main_folder = '"Политики"'
            add_case = 63262
    #Основная таблица
    elif under_folder == 3:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Основная таблица - 0\nФильтры - 1\nПанель - "Бар" - 2\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Политики"'
            add_case = 63259
        elif under_under_folder == 1:
            main_folder = '"Политики"'
            add_case = 63247
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63278
    #Правила
    elif under_folder == 4:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Правила - 0\nОсновное - 1\nУсловия - 2\nДействия - 3\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Политики"'
            add_case = 63244
        elif under_under_folder == 1:
            main_folder = '"Политики"'
            add_case = 63263
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63264
        elif under_under_folder == 3:
            under_under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Действия - 0\nТаблица "Действия" - 1\n >>>  '))
            if under_under_under_folder == 0:
                main_folder = '"Политики"'
                add_case = 63248
            elif under_under_under_folder == 1:
                main_folder = '"Политики"'
                add_case = 63267
    #Слои
    elif under_folder == 5:
        main_folder = '"Политики"'
        add_case = 63245
    #Верстка
    elif under_folder == 6:
        under_under_folder = int(input('\nВыбрать подпапку:\nОстаться в Верстка - 0\nТаблица список политик - 1\nОкно проверка политики - 2\nБыстрые фильтры - 3\nПравый фрейм - 4\nОкно расписание - 5\nОкно условия - 6\nЛевый фрейм - 7\n >>>  '))
        if under_under_folder == 0:
            main_folder = '"Политики"'
            add_case = 63279
        elif under_under_folder == 1:
            main_folder = '"Политики"'
            add_case = 63597
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63596
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63599
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63598
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63600
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 63601
        elif under_under_folder == 2:
            main_folder = '"Политики"'
            add_case = 64618
        
        
        
class Click:
    def __init__(self, xpath):
        self.xpath = xpath
    def func_click(self):
        return browser.find_element(By.XPATH, self.xpath).click()

class Input:
    def __init__(self, xpath, text):
        self.xpath = xpath
        self.text = text
    def func_input(self):
        return browser.find_element(By.XPATH, self.xpath).send_keys(self.text)


# --------------------------------------------------------------------------------------------------- JIRA

url_jr = 'http://tracker.zecurion.local:8080/browse/'
browser = webdriver.Chrome()
browser.get(url_jr)
browser.maximize_window()
time.sleep(0)

# принимаем куки
button_1 = Click('//a')
button_1.func_click()
time.sleep(0)

# указываем логин
field_name = Input('//*[@id="login-form-username"]', 'Kochetov')
field_name.func_input()
time.sleep(0)

# указываем пароль
field_password = Input('//*[@id="login-form-password"]', 'Diego22363469')
field_password.func_input()
time.sleep(0)


# log_in
button_1 = Click('//*[@id="login"]')
button_1.func_click()
time.sleep(1)


# ------------------------------------------------------------------------------------------------- JIRA 2

field_password = Input('//input[@id="quickSearchInput"]', '{}'.format(bug_number))
field_password.func_input()
browser.find_element(By.XPATH, '//input[@id="quickSearchInput"]').send_keys(Keys.ENTER)
time.sleep(0)


# копируем и присваиваем STR
bug_name = browser.find_element(By.XPATH, '//h1[@id="summary-val"]').text
str_text = browser.find_element(By.XPATH, '//div[@class="user-content-block"]/ol').text
result_text = browser.find_element(By.XPATH, '//div[@class="user-content-block"]/p[2]').text
expected_text = browser.find_element(By.XPATH, '//div[@class="user-content-block"]/p[3]').text
# tr_bug_number = browser.find_element(By.XPATH, '//div[@class="content-header-id"]').text



result = 'Failed'

# browser.close()
# browser.quit()

default_text = 'Windows 10\nChrome ver auto\nВход в систему NGFW'


# -------------------------------------------------------------------Вход в TestRail
url_tr = 'http://192.168.0.191/testrail/index.php?/auth/login/'
browser = webdriver.Chrome()
browser.get(url_tr)
browser.maximize_window()
time.sleep(0)

# name
field_name = Input('//input[@id="name"]', 'Evgeniy.Kochetov@zecurion.com')
field_name.func_input()
time.sleep(0)

# password
field_name = Input('//input[@id="password"]', 'Diego22363469')
field_name.func_input()
time.sleep(0)

# click_log_in
button1 = Click('//button[contains(text(),"Log")]')
button1.func_click()
time.sleep(0)

# click_NGFW
button1 = Click('//a[contains(text(), "NGFW")]')
button1.func_click()
time.sleep(0)

# click_Test Suites & Cases
button1 = Click('//a[contains(text(), "Test Suites & Cases")]')
button1.func_click()
time.sleep(0)


# -------------------------------------------------------------------Вход по папкам

button_folder = browser.find_element(By.XPATH, '//a[contains(text(),{})]'.format(main_folder))
button_folder.click()
time.sleep(1)

button_addcase = browser.find_element(By.XPATH, '//a[@onclick="this.blur(); App.Cases.add({}); return false;"]'.format(add_case))
button_addcase.click()
time.sleep(1)

# while not browser.find_element_by_name(bug_name):
button_addcase = browser.find_element(By.XPATH, '//div[@rel="{}"]//input[@class="form-control form-control-large form-control-inline title"]'.format(add_case))
button_addcase.send_keys(bug_name)
time.sleep(1)


# ------------------------------------------------------------------------------- Принимаем титл в тр
title_button = Click('//div[@id="inlineSectionAddCase-{}"]//input[@class="submit"]'.format(add_case))
title_button.func_click()
time.sleep(1)

title_button = Click('//div[@id="inlineSectionAddCase-{}"]//img[@src="images/icons/cancel.png"]'.format(add_case))
title_button.func_click()
time.sleep(1)

title_button2 = Click('''//span[contains(text(),'{}')]'''.format(bug_name))
title_button2.func_click()
time.sleep(1)


# ------------------------------------------------------------------------------- Заполняем баг в тр
# edit_click
edit_button = Click('//a[contains(@class,"toolbar-button")]')
edit_button.func_click()
time.sleep(0)

# click template
edit_button = Click('//select[@id="template_id"]')
edit_button.func_click()
time.sleep(1)

# choose_template
edit_button = Click('//option[@value="2"]')
edit_button.func_click()
time.sleep(1)

# input_preconditions
button_addcase = Input('//textarea[@id="custom_preconds"]', default_text)
button_addcase.func_input()
time.sleep(1)

# click_addstep
edit_button = Click('//a[@class="addStep"]')
edit_button.func_click()
time.sleep(1)

# input_step_description
button_left_column = Input('//textarea[@placeholder="Step Description"]', str_text)
button_left_column.func_input()
time.sleep(1)

# input_expected_result
button_left_column = Input('//textarea[@placeholder="Expected Result"]', '{}\n\n{}'.format(result_text, expected_text))
button_left_column.func_input()
time.sleep(1)

# click_save_test_case
edit_button = Click('//button[@id="accept"]')
edit_button.func_click()
time.sleep(0)


# # ------------------------------------------------------------------------------- Переходим в Test Runs & Results
# click_Test Suites & Cases
button1 = Click('//a[contains(text(), "Test Runs & Results")]')
button1.func_click()
time.sleep(1)

button2 = Click('//a[contains(text(),{})]'.format(main_folder))
button2.func_click()
time.sleep(1)

if main_folder == '"Политики"':
    button2 = Click('//a[contains(text(),{})]'.format(main_folder))
    button2.func_click()
    time.sleep(1)

button3 = Click('''//a[contains(text(),'{}')]'''.format(bug_name))
button3.func_click()
time.sleep(1)

# заполнение результата в tr
button4 = Click('//a[@id="addResult"]')
button4.func_click()
time.sleep(1)

field5 = Input('//select[@id="addResultStatus"]', result)
field5.func_input()
time.sleep(1)

field6 = Input('//select[@id="addResultAssignTo"]', 'Me')
field6.func_input()
time.sleep(1)

field7 = Input('//input[@id="addResultVersion"]', 'Proxy 4.0')
field7.func_input()
time.sleep(1)

field8 = Input('//input[@id="addResultDefects"]', 'PROXY-' + str(bug_number))
field8.func_input()
time.sleep(1)

field9 = Input('//select[@class="form-control form-select"]', result)
field9.func_input()
time.sleep(1)

button10 = Click('//button[@id="addResultSubmit"]')
button10.func_click()
time.sleep(1)

tr_bug_number_2 = browser.find_element(By.XPATH, '//div[@class="content-header-id"]').text
time.sleep(1)

# ------------------------------------------------------------------- создание и запись файла

text = '\nBug name: {}\nBug number: {}\nTest rail number: {}\n'.format(bug_name, bug_number, tr_bug_number_2)

with open('C:\\Users\\kochetov\\Desktop\\auto-bug.txt', 'a', encoding='utf-8') as file:
    file.write(text)


















































