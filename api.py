from utils.http_methods import Http_methods

"""Методы для тестирования google maps api"""

base_url = 'https://rahulshettyacademy.com'
key = '?key=qaclick123'


class Api_methods():

    # метод для создания новой локации
    @staticmethod
    def post_method():
        post_resourse = '/maps/api/place/add/json'
        post_json = {
            "location": {
                "lat": -38.383494,
                "lng": 33.427362
            }, "accuracy": 50,
            "name": "Frontline house",
            "phone_number": "(+91) 983 893 3937",
            "address": "29, side layout, cohen 09",
            "types": [
                "shoe park",
                "shop"
            ],
            "website": "http://google.com",
            "language": "French-IN"
        }

        post_url = base_url + post_resourse + key
        print(post_url)
        post_result = Http_methods.post(post_url, post_json)
        print(post_result.text)
        return post_result

    # метод для проверки новой локации
    @staticmethod
    def get_method(place_id):
        get_resourse = '/maps/api/place/get/json'
        get_url = base_url + get_resourse + key + '&place_id=' + place_id
        print(get_url)

        get_result = Http_methods.get(get_url)
        print(get_result.text)
        return get_result

    # метод для изменения новой локации
    @staticmethod
    def put_method(place_id):
        put_resourse = '/maps/api/place/update/json'
        put_url = base_url + put_resourse + key
        print(put_url)
        put_json = {
            "place_id": place_id,
            "address": "100 Lenina street, RU",
            "key": "qaclick123"
        }
        put_result = Http_methods.put(put_url, put_json)
        print(put_result.text)
        return put_result

    # метод для удаления новой локации
    @staticmethod
    def del_method(place_id):
        del_resourse = '/maps/api/place/delete/json'
        del_json = {
            "place_id": place_id
        }
        del_url = base_url + del_resourse + key
        print(del_url)
        del_result = Http_methods.delete(del_url, del_json)
        print(del_result.text)
        return del_result