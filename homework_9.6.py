# # Задача 1. Сумма чисел 2
import os

def func_read(file):
    s = []
    for i in file:
        g = [int(f) for f in i if f.isdigit()]
        s += g
    return sum(s)


file_1 = open('C:/Users/kochetov/txt/Practice module 9.6/numbers.txt', 'r', encoding = 'utf-8')
with open('C:/Users/kochetov/txt/Practice module 9.6 (double)/answer.txt', 'w', encoding = 'utf-8') as f:
    f.write(str(func_read(file_1)))


func_read(file_1)




# Задача 2. Дзен Пайтона
import os
s = []

file = open('C:/Users/kochetov/txt/Practice module 9.6/zen.txt', 'r', encoding = 'utf-8')
for i in file:
    s.append(i)
    s.reverse()
    
for i in s:
    print(i, end = '')



# Задача 3. Дзен Пайтона 2
import os

def func_alphas(f):
    f = open(f, 'r', encoding='utf-8')
    alpha = 0
    for i in f:
        for j in i:
            if j.isalpha():
                alpha += 1
    f.close()
    return alpha

def func_words(f):
    f = open(f, 'r', encoding='utf-8')
    words = 0
    for i in f:
        for j in i:
            if j == ' ':
                words += 1
    return words                                     # разобраться, как посчитать кол-во слов в тексте

def func_string(f):
    f = open(f, 'r', encoding='utf-8')
    strings = 0
    for i in f:
        if i.rstrip():
            strings += 1
    f.close()
    return strings


file = 'C:/Users/kochetov/txt/Practice module 9.6/zen.txt'


print('Количество букв в тексте:', func_alphas(file))
print('Количество строк в тексте:', func_string(file))
print('Количество слов в файле: ', func_words(file))


# Задача 4. Файлы и папки

import os
def func(f):
    dir_count = 0
    file_count = 0
    size_count = 0
    for i in os.listdir(f):
        way = os.path.join(f, i)
        if os.path.isdir(way):
            dir_count += 1
        elif os.path.isfile(way):
            file_count += 1
            size_count += os.path.getsize(way)
    return dir_count, file_count, size_count
            
a_path = input('Введите путь: ')
z1, z2, z3 = func(a_path)
print('Количество подкаталогов: {} \nКоличество файлов: {} \nРазмер всех файлов: {} байт'.format(z1, z2, z3))



# Задача 5. Сохранение
import os

def func1(way):
    s_path = ''
    for i in way:
        s_path += i + '/'
    d_path = 'C:/' + s_path
    return d_path

def func2(way2, f, t):
    os.chdir(way2)
    if os.path.exists(f):
        print('Файл с таким названием уже есть в папке')
    else:
        with open(way2 + f, 'w') as file:
            file.write(t)
    
text = input('Введите текст: ')
a_path = input('Введите папки пути через пробел: ').split()
file = input('Введите название файла: ') + '.txt'

func1(a_path)
func2(func1(a_path), file, text)



# Задача 7. Турнир
import os
first_file = 'C:/Users/kochetov/txt/Practice module 9.6/module7/first_tour.txt'
max_num = ''
s = []
g = []
k = []
count_players = 0

#чтение файла
a_file = open(first_file, 'r', encoding='utf-8')

#определение наибольшего параметра
for i in a_file:
    max_num += i
    break
max_num = int(max_num)

#вывод неотсортированного списка, подходящего под наибольший параметр
for i in a_file:
    if int(i.split()[-1]) > max_num:
        g.append(i.split())
        count_players += 1

#выбор баллов участников
for i in g:
    s.append(int(i[-1]))
    
#сортировка баллов участников 
s.sort()
s.reverse()

#сбор значений согласно сортировке баллов участников
for m in s:
    for n in g:
        if m == int(n[-1]):
            k.append(n)

#построчная запись отсортированных участников
with open('C:/Users/kochetov/txt/Practice module 9.6/module7/second_tour.txt', 'w', encoding='utf-8') as file:
    # file.write(count_players)
    for i, (one, two, three) in enumerate(k):
        file.write('{}) {} {} {}\n'.format(i + 1, one, two, three))

a_file.close()




    
    
































































