# Заполнить имя, пароль
# Статус

import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains, Keys


s = []

file = open('C:\\Users\\kochetov\\Desktop\\testrail.txt', 'r', encoding='utf-8')
while True:
    line = file.readline()
    if not line:
        break
    s.append(line.strip())


url = 'http://192.168.0.191/testrail/index.php?/auth/login/'
browser = webdriver.Chrome()
browser.get(url)
browser.maximize_window()
time.sleep(0)


class Click:
    def __init__(self, xpath):
        self.xpath = xpath
    def func_click(self):
        return browser.find_element(By.XPATH, self.xpath).click()

class Input:
    def __init__(self, xpath, text):
        self.xpath = xpath
        self.text = text
    def func_input(self):
        return browser.find_element(By.XPATH, self.xpath).send_keys(self.text)
        

# вход в систему tr
input_1 = Input('//input[@id="name"]', 'Evgeniy.Kochetov@zecurion.com')
input_1.func_input()
time.sleep(0)

input_2 = Input('//input[@id="password"]', 'Diego22363469')
input_2.func_input()
time.sleep(0)

button_1 = Click('//button[contains(text(),"Log in")]')
button_1.func_click()
time.sleep(0)
            
            
# выбор проекта NGFW
button_1 = Click('//div[@class="chart-legend-name text-ppp"]//a[text()="NGFW"]')
button_1.func_click()
time.sleep(0)
            
button2 = Click('//a[contains(text(), "Test Runs & Results")]')
button2.func_click()
time.sleep(0)     


for i in s:
    # input номера в поиск
    input_2 = Input('//input[@class="top-search-control"]', '{}'.format(i))
    input_2.func_input()
    browser.find_element(By.XPATH, '//input[@class="top-search-control"]').send_keys(Keys.ENTER)
    time.sleep(0)

    # click addResult
    browser.find_element(By.XPATH, '//a[@id="addResult"]').send_keys(Keys.ENTER)
    time.sleep(1)

    # проверка и заполнение номера PROXY при наличии такового
    # заполение окна addResult
    def func_addResult():
        field6 = Input('//select[@id="addResultAssignTo"]', 'Me')
        field6.func_input()
        time.sleep(1)

        field7 = Input('//input[@id="addResultVersion"]', 'Proxy 4.0')
        field7.func_input()
        time.sleep(1)

        field8 = Input('//input[@id="addResultDefects"]', str(proxy_number))
        field8.func_input()
        time.sleep(1)
            
        field9 = Input('//select[@class="form-control form-select"]', 'Failed')
        field9.func_input()
        time.sleep(1)
            
        button10 = Click('//button[@id="addResultSubmit"]')
        button10.func_click()
        time.sleep(1)
            
    try:
        proxy_number = browser.find_element(By.XPATH, '//a[@class="defectLink"]').text
        func_addResult()
            
    except:
        print('Ошибка: У бага (номер в TestRail: {}) Не указан номер PROXY'.format(i))
        proxy_number == ''
        func_addResult()

