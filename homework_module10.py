# Задача 1. Имена 2

file = 'C:/Users/kochetov/txt/homework 10/Task 1/people.txt'
file_read = open(file, 'r', encoding='utf-8')

s = 0
b = []

for i in file_read:
    for j in i:
        if j.isalpha():
            s += 1
    b.append(s)
    s = 0

str_count = 0
hold_count = 0
for i in b:
    str_count += 1
    if i < 3:
        hold_count = str_count
        print('\nОшибка: менее трех символов в строке {}'.format(hold_count))

print('Общее кол-во символов:', sum(b))

with open('C:/Users/kochetov/txt/homework 10/Task 1/' + 'errors.log', 'w', encoding='utf-8') as f:
    f.write('\nОшибка: менее трех символов в строке {}'.format(str(hold_count)))
    f.write('\nОбщее кол-во символов: {}'.format(str(sum(b))))




# Задача 2. Координаты
import random

a_path = 'C:/Users/kochetov/txt/homework 10/Task 2'
a_file = open(a_path + '/coordinates.txt', 'r', encoding='utf-8')


def func_random_plus(a, b):
    a += random.randint(0, 5)
    b += random.randint(0, 10)
    return a / b

def func_random_minus(a, b):
    a -= random.randint(0, 5)
    b -= random.randint(0, 10)
    return b / a
try:
    s = []
    m = []
    for i in a_file:
        list_alphas = i.split()
        
    for i in list_alphas:
        s.append(int(i))
        
    one = func_random_plus(s[0],  s[1])
    two = func_random_minus(s[0], s[1])
    k = one, two, random.randint(0, 100)
    print('Передаваемые данные в файл: ', k)
    file_create = open(a_path + '/result.txt', 'w', encoding='utf-8')


    with open(a_path + '/result.txt', 'w', encoding='utf-8') as f:
        for i in k:
            f.write(str(i))
            f.write(' ')
except(TypeError):
    print('Запись в файл возможна только в формате "строка"')



# Задача 3. Счастливое число
import random
a_path = 'C:/Users/kochetov/txt/homework 10/Task 3/out_file.txt'
start_num = 0

while True:
    lucky_num = random.randint(1, 13)
    if lucky_num == 13:
        print('Вас постигла неудача!')
        break
    else:
        x = int(input('Введите число: '))
        with open(a_path, 'a', encoding='utf-8') as f:
            f.write(str(x))
            f.write('\n') 
        start_num += x
        if start_num >= 777:
            print('Вы успешно выполнили задание!')
            break
        


# Задача 4. Регистрация


import re

a_file = 'C:/Users/kochetov/txt/homework 10/Task 4/registrations.txt'
file_read = open(a_file, 'r', encoding='utf-8')
file_write_1 = 'C:/Users/kochetov/txt/homework 10/Task 4/registrations_good.log'
file_write_2 = 'C:/Users/kochetov/txt/homework 10/Task 4/registrations_bad.log'



def func_tree_parts(file, file_2, file_3):

    s = []
    for i in file:
        s.append(i.split())
    for i in s:
        if len(i) != 3:
            # print('В строке {} отсутствуют данные'.format(i))
            with open(file_3, 'a', encoding='utf-8') as f:
                f.write(str(i))
                f.write(' ')
                f.write(' ~~~ Отсутствует одно, или несколько полей'.format(i))
                f.write('\n')
        


def func_name_part(file, file_2, file_3):
    file = open(a_file, 'r', encoding='utf-8')
    s = []
    for i in file:
        s.append(i.split())
    for i in s:
        for j in i[0]:
            if not j.isalpha():
                # print('В имени {}, строки {} есть недопустимые символы'.format(i[0], i))
                with open(file_3, 'a', encoding='utf-8') as f:
                    f.write(str(i))
                    f.write(' ')
                    f.write(' ~~~ В имени {} есть недопустимые символы'.format(i[0]))
                    f.write('\n')



def func_email_part(file, file_2, file_3):
    file = open(a_file, 'r', encoding='utf-8')
    s = []
    for i in file:
        s.append(i.split())
    for i in s:
        for j in i[1]:
            email = i[1]
            pattern = r'^[-\w\.]+@([-\w]+\.)+[-\w]{2,4}$'
            if re.match(pattern, email) is None:
                with open(file_3, 'a', encoding='utf-8') as f:
                    f.write(str(i))
                    f.write(' ')
                    f.write(' ~~~ Поле e-mail является невалидным')
                    f.write('\n')
                    break
                
                
# def func_positive_test(one, two, three):
#     if not one and not two and not three:
#         print('will be all right')
#         file = open(a_file, 'r', encoding='utf-8')
#         s = []
#         for i in file:
#             s.append(i.split())
#         for i in s:
            
#             with open(file_3, 'a', encoding='utf-8') as f:





# Задача 5. Текстовый калькулятор
file = 'C:/Users/kochetov/txt/homework 10/Task 5/calc.txt'
file_read = open(file, 'r', encoding='utf-8')

def func_moves(num_1, symbol, num_2):
    summ = 0
    if symbol == '+':
        summ += (int(num_1) + int(num_2))
    elif symbol == '-':
        summ += (int(num_1) - int(num_2))
    elif symbol == '*':
        summ += (int(num_1) * int(num_2))
    elif symbol == '/':
        summ += (int(num_1) / int(num_2))
    return summ

def func_summa():
    summa = 0
    for i in file_read:
        i = i.split()
        if (i[0].isdigit() and i[2].isdigit()) and (i[1] == '+' or i[1] == '-' or i[1] == '/' or i[1] == '*'):
                summa += func_moves(i[0], i[1], i[2])
        else:
            func_correct(i, file)
    return summa
    
    
def func_correct(string, file):
    correct_string = ' '.join(string)
    print('\nВ строке {} есть ошибка'.format(correct_string))
    answer = input('Хотите исправить? \n "да" / "нет"  >>>   ').lower()
    if answer == 'да':
        new_str = input('Введите исправленную строку: ')
        
        super_file = open(file, 'r')
        data = super_file.read()
        data = data.replace(correct_string, new_str)
        super_file.close()
        super_file = open(file, 'w')
        super_file.write(data)
        super_file.close()
        func_summa()
    
        
    else:
        print('Ошибка не исправлена')
        
        
print(func_summa())

























# import re

# a_file = 'C:/Users/kochetov/txt/homework 10/Task 4/registrations.txt'
# file_read = open(a_file, 'r', encoding='utf-8')
# file_write_1 = 'C:/Users/kochetov/txt/homework 10/Task 4/registrations_good.log'
# file_write_2 = 'C:/Users/kochetov/txt/homework 10/Task 4/registrations_bad.log'



# def func_tree_parts(file, file_3):

#     s = []
#     for i in file:
#         s.append(i.split())
#     for i in s:
#         if len(i) != 3:
#             # print('В строке {} отсутствуют данные'.format(i))
#             with open(file_3, 'a', encoding='utf-8') as f:
#                 f.write(str(i))
#                 f.write(' ')
#                 f.write(' ~~~ Отсутствует одно, или несколько полей'.format(i))
#                 f.write('\n')
        
        


# def func_name_part(file, file_3):
#     file = open(a_file, 'r', encoding='utf-8')
#     s = []
#     for i in file:
#         s.append(i.split())
#     for i in s:
#         for j in i[0]:
#             if not j.isalpha():
#                 # print('В имени {}, строки {} есть недопустимые символы'.format(i[0], i))
#                 with open(file_3, 'a', encoding='utf-8') as f:
#                     f.write(str(i))
#                     f.write(' ')
#                     f.write(' ~~~ В имени {} есть недопустимые символы'.format(i[0]))
#                     f.write('\n')
            


# def func_email_part(file, file_3):
#     file = open(a_file, 'r', encoding='utf-8')
#     s = []
#     for i in file:
#         s.append(i.split())
#     for i in s:
#         for j in i[1]:
#             email = i[1]
#             pattern = r'^[-\w\.]+@([-\w]+\.)+[-\w]{2,4}$'
#             if re.match(pattern, email) is None:
#                 with open(file_3, 'a', encoding='utf-8') as f:
#                     f.write(str(i))
#                     f.write(' ')
#                     f.write(' ~~~ Поле e-mail является невалидным')
#                     f.write('\n')
#                     break
                
                
                
                
                
# func_tree_parts(file_read, file_write_2)
# func_name_part(file_read, file_write_2)
# func_email_part(file_read, file_write_2)








# def func_positive_test(one, two, three):
#     if not one and not two and not three:
#         print('will be all right')
#         file = open(a_file, 'r', encoding='utf-8')
#         s = []
#         for i in file:
#             s.append(i.split())
#         for i in s:
            
#             with open(file_3, 'a', encoding='utf-8') as f:





# file = 'C:/Users/kochetov/txt/homework 10/Training/test.txt'

# file_read = open(file, 'r')
# data = file_read.read()
# data = data.replace('jest', 'ji-est')
# file_read.close()
# file_read = open(file, 'w')
# file_read.write(data)
# file_read.close()






















































































































































































































