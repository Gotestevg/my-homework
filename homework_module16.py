# МОДУЛЬ 16
# Задача 1. Страшный код

# a = [1, 5, 3]
# b = [1, 5, 1, 5]
# c = [1, 3, 1, 5, 3, 3]

# print('Количесто цифр 5 при первом объединении:', (a + b).count(5))
# print('Количество цифр 3 при втором объединении:', (a + b + c).count(5))
# print('Итоговый список:', ([elem for elem in (a + b) if elem != 5] + c))



# Задача 2. Шеренга

# classOne = list(range(160, 172, 2))
# classTwo = list(range(162, 180, 3))

# final = (classOne + classTwo)
# final.sort()
# print('Отсортированный список учеников:', final)

# # если необходимо вывести уникальные значения
# print(list(set(final)))



# Задача 3. Детали
# shop = [['каретка', 1200], ['шатун', 1000], ['седло', 300], 
#         ['педаль', 100], ['седло', 1500], ['рама', 12000], 
#         ['обод', 2000], ['шатун', 200], ['седло', 2700]]
# x = input('Введите название детали: ')
# s1 = 0
# s2 = 0
# for i in shop:
#     for j in i:
#         if j == x:
#             s1 += 1
#             s2 += (i[-1])
        
# print('Количество деталей:', s1)
# print('Общая стоимость:', s2 )



# Задача 4. Вечеринка
# guests = ['Петя', 'Ваня', 'Саша', 'Лиза', 'Катя']

# while True:
#         print('\nСейчас на вечеринке человек:', len(guests))
#         x = input('Гость пришел, или ушел? ')
#         if x == 'Пора спать' or x == 'пора спать':
#                 print('Вечеринка закончилась, все легли спать')
#                 break
#         n = input('Имя гостя: ')
#         if x == 'пришел' or x == 'Пришел':
#             if len(guests) == 6:
#                 print('Прости,', n, ',но мест нет')
#             else:
#                 guests += [n]
#                 print('Привет,', n, '!')
            
#         elif x == 'ушел' or x == 'Ушел':
#             if n in guests:
#                 guests.remove(n)
#                 print('Пока,', n, '!')
#             elif n not in guests:
#                 print('Гостя с таким именем не было на вечеринке')



# Задача 5. Песни
# z = [
#     ['World in My Eyes', 4.86], ['Sweetest Perfection', 4.43], ['Personal Jesus', 4.56], 
#     ['Halo', 4.9], ['Waiting for the Night', 6.07], ['Enjoy the Silence', 4.20], 
#     ['Policy of Truth', 4.76], ['Blue Dress', 4.29], ['Clean', 5.83]
# ]

# s = []
# for i in z:
#     for j in i:
#         s += [j]

# count = int(input('Сколько песен выбрать? '))
# musicTime = 0
# for i in range(1, count + 1):
#     print('Название', i, 'песни: ', end = '')
#     x = input()
#     if x in s:
#         musicTime += s[s.index(x) + 1]
# print('Общее время звучания песен:', round(musicTime, 2), 'минуты')




# Задача 6. Уникальные элементы
# s1 = []
# s2 = []
# for i in range(1, 4):
#     print('Введите', i, '- е число для первого списка: ', end = '')
#     x = int(input())
#     s1 += [x]
# for i in range(1, 8):
#     print('Введите', i, '- е число для второго списка: ', end = '')
#     x = int(input())
#     s2 += [x]
    
# print('\nПервый список:', s1)
# print('Второй список:', s2)
# print('\nНовый первый список с уникальными элементами:', list(set((s1 + s2))))




# Задача 7. Ролики
# countR = int(input('Кол-во коньков: '))
# countP = int(input('Кол-во людей: '))
# s1 = []
# s2 = []

# for i in range(1, countR + 1):
#     print('Размер', i, '- й пары: ', end = '')
#     x = int(input())
#     s1 += [x]
# for i in range(1, countP + 1):
#     print('Размер ноги', i, '- го человека: ', end = '')
#     x = int(input())
#     s2 += [x]
# f = [elem for elem in s1 if elem in s2]

# print('Наибольшее кол-во людей, которые могут взять ролики:', len(f))



# Задача 8. Считалка
# people_cnt = int(input('Кол-во человек: '))
# drop_number = int(input('Какое число в считалке? '))
# print('Значит, выбывает каждый', drop_number, 'человек')

# people_list = []
# for i_num in range(1, people_cnt + 1):
#     people_list.append(i_num)

# index_del = 0
# while len(people_list) > 1:
#     print('Текущий круг людей:', people_list)
    
    
#     index_start = index_del % len(people_list)
#     print('Начало счёта с номера', people_list[index_start])

#     index_del = (index_start + drop_number - 1) % len(people_list)
#     print('Выбывает человек под номером', people_list[index_del])
#     people_list.remove(people_list[index_del])

# print('\nОстался человек под номером', people_list[0])




# Задача 9. Друзья (баг: последние показатели возвращает х2)
# c_friends = int(input('Количество друзей: '))
# c_papers = int(input('Количество расписок: '))
# s = []

# for i in range(1, c_friends + 1):
#     s += [0]

# def func1(frm, to, how_mutch, s):
#     s[frm - 1] += how_mutch
#     s[to - 1] -= how_mutch
#     return s

# def func0():
#     for i in range(1, c_papers + 1):
#         print(i, end = '-я расписка:\n')
#         frm = int(input('От кого: '))
#         to = int(input('Кому: '))
#         how_mutch = int(input('Сколько: '))
#         func1(frm, to, how_mutch, s)

#     print('Баланс друзей: ', func1(frm, to, how_mutch, s))



# func0()




# Задача 10. Симметричная последовательность
# count = int(input('Кол-во чисел: '))
# s = []
# g = []
# k = 0
# # создаем два идентичных списка из получаемых значений
# for i in range(count):
#     x = int(input('Число: '))
#     s += [x]
#     g += [x]
# # разворачиваем один из них
# g.reverse()
# # считаем количество одинаковых значений с краю списка
# for i in g:
#     if i == g[0]:
#         k += 1
#     else:
#         break
# # убираем одно, или несколько одинаковых значений от края списка
# if k > 1:
#     print('\nПоследовательность:', s)
#     print('Нужно приписать чисел:', len(s) - k)
#     print('Сами числа:', g[k:])
# else:
#     print('\nПоследовательность:', s)
#     print('Нужно приписать чисел:', len(s) - 1)
#     print('Сами числа:', g[1:])

















































