# Задача 7. Совместное проживание
import random

class human_model: 
    name = 'John'
    sitost = 50

    def func_eat(self):
        house_model.food -= 10
        self.sitost += 10

    def func_work(self):
        house_model.money += 10
        self.sitost -= 10

    def func_play(self):
        self.sitost -= 10

    def go_to_shop(self):
        house_model.food += 10
        house_model.money -= 10


class house_model:
    food = 50
    money = 0



for i in range(1, 7):
    print('День {}-й'.format(i))
    kubik = random.randint(1, 6)
    if human_model.sitost <= 0:
        print('Человек умер')
    elif human_model.sitost <= 20:
        human_model.func_eat()
    elif house_model.food <= 10:
        human_model.go_to_shop()
    elif house_model.money < 50:
        human_model.func_work()
    elif kubik == 1:
        human_model.func_work()
    elif kubik == 2:
        human_model.func_eat()
    else:
        human_model.func_play()
