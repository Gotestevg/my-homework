# Задача_1. Информация о системе
import platform
import sys

info = 'OS info is \n{}\n\nPython version is {} {}'.format(
    platform.uname(),
    sys.version,
    platform.architecture(),
)
print(info)

with open('os_info.txt', 'w', encoding = 'utf8') as file:
    file.write(info)
    


# Задача_2. Сессия
while True:
    print("Введите первую точку")
    x1 = float(input('X: '))
    y1 = float(input('Y: '))
    print("\nВведите вторую точку")
    x2 = float(input('X: '))
    y2 = float(input('Y: '))
    if x1 == x2:
        print('\nЗначения х1 и х2 должны отличаться')
    else:
        break

x_diff = x1 - x2
y_diff = y1 - y2
k = y_diff / x_diff
b = y2 - k * x2

print("Уравнение прямой, проходящей через эти точки:")
print("y = ", k, " * x + ", b)



# Задача 3. Сумма и разность
def func0():
    return sum([int(i) for i in str(x)])
def func1():
    return len(str(x))

x = int(input('Введите число: '))
print('Сумма цифр:', func0())
print('Количество цифр в числе:', func1())
print('Разность суммы и количества цифр:', func0() - func1())



# Задача_4. Число наоборот 3
def func0(a):
    a = [_ for _ in str(a)]
    ind = a.index('.')
    left = a[:ind]
    right = a[ind + 1:]
    left.reverse()
    right.reverse()
    return float(''.join(left + ['.'] + right))

x = float(input('Введите первое число: '))
y = float(input('Введите второе число: '))
print('Первое число наоборот:', func0(x))
print('Второе число наоборот:', func0(y))
print('Сумма:', func0(x) + func0(y))



# Задача 5. Наименьший делитель
x = int(input('Введите число: '))
y = 2

while x % y != 0:
    y += 1
print(y)



# Задача 6. Монетка 2
import math
print('Введите координаты монетки: ')
x = float(input('X: '))
y = float(input('Y: '))
r = int(input('Введите радиус: '))

if math.sqrt(x ** 2 + y ** 2) > r:
    print('Монетки в области нет')
else:
    print('Монетка где-то рядом')



# Задача 7. Годы
def func2(x, y):
    s = 0
    print('Годы от', x, 'до', y, 'с тремя одинаковыми цифрами:')
    for i in range(x, y + 1):
        i = [g for g in str(i)]
        for j in i:
            if i.count(j) == 3:
                s += 1
                print(''.join(i))
                break
    if s == 0:
        print('В выбранном интервале таких годов нет')
                
def func1(x, y):
    if x >= y:
        print('Начальный год должен быть меньше конечного\n')
        func0()
    elif len(str(x)) != 4 or len(str(y)) != 4:
        print('Год должен состоять из 4х цифр\n')
        func0()
    else:
        func2(x, y)

def func0():
    a = int(input('Введите начальный год: '))
    b = int(input('Введите конечный год: '))
    func1(a, b)

func0()


































