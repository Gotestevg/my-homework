# Задача 1. Гласные буквы
text = input('Введите текст: ')
f = ([i for i in text if i in ['а', 'о', 'и', 'е', 'ё', 'э', 'ы', 'у', 'ю', 'я']])
print('Список гласных букв:', f)
print('Длина списка:', len(f))



# Задача 2. Генерация
print([i % 5 if i % 2 != 0 else 1 for i in range(int(input('Введите длину списка: ')))])



# Задача 3. Случайные соревнования
import random
a = [round(random.uniform(5, 10), 2) for i in range(20)]
b = [round(random.uniform(5, 10), 2) for i in range(20)]
print(a)
print(b)
print(list(map(max, a, b)))


# Задача 4. Тренируемся со срезами
s = 'abcdefg'

print(s[:])
print(s[::-1])
print(s[0::2])
print(s[1::2])
print(s[:1])
print(s[1::-1])



# Задача 5. Разворот
s = input('Введите строку: ')
print((s[s.index('h') + 1:s.rindex('h')])[::-1])



# Задача 6. Сжатие списка
import random
s = [random.randint(0, 2) for i in range(10)]
print('Список до сжатия:', s)
print('Список отсортированный:', [i for i in s if i > 0] + [i for i in s if i == 0])
print('Список после сжатия:', [i for i in s if i > 0])



# Задача 7. Двумерный список
print([[i for i in range(s, 13, 4)] for s in range(1, 5)])


# Задача 8. Развлечение
import random

s = int(input('Кол-во палок: '))
f = int(input('Кол-во бросков: '))
g = [i for i in range(1, s + 1)]
m = []
v = []

for i in range(f):
    print('\nБросок', i + 1, end = '.\n')
    a = random.randint(1, s)
    b = (random.randint(a, s))
    print('Сбиты палки с номера:', a, 'по номер:', b)
    c = [i for i in range(a, b + 1)]
    m.extend(c)

k = [i for i in g if i not in m]

for i in range(1, s + 1):
    if i in k:
        v.append('l')
    else:
        v.append('.')
print('Результат:', ''.join(v))



# Задача 9. Список списов
s = [[[1, 2, 3], [4, 5, 6], [7, 8, 9]], [[10, 11, 12], [13, 14, 15], [16, 17, 18]]]
print([y for x in s for y in [b for a in x for b in a]])



# Задача 10. Шифр Цезаря
s = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
text = input('Введите сообщение: ')
step = int(input('Сдвиг: '))
d = []

for i in text:
    if i != ' ':
        f = s.index(i)
        if f + step > 30:
            d.append(s[f + step - 33])
        else:
            d.append(s[f + step])
    else:
        d.append(' ')
print(''.join(d))






























































