# Задача 1. Песни 2
violator_songs = {
    'World in My Eyes': 4.86,
    'Sweetest Perfection': 4.43,
    'Personal Jesus': 4.56,
    'Halo': 4.9,
    'Waiting for the Night': 6.07,
    'Enjoy the Silence': 4.20,
    'Policy of Truth': 4.76,
    'Blue Dress': 4.29,
    'Clean': 5.83
}

count = int(input('Сколько песен выбрать? '))
sum_time = 0

for i in range(1, count + 1):
    print('Песня №', i, end = ' ')
    x = input('')
    sum_time += violator_songs[x]

print('Общее время звучания песен:', round(sum_time, 2))



# Задача 2. География
# Мой вариант решения
count = int(input('Сколько стран необходимо добавить? '))
country_list = []
city_list = []
final_dict = dict()

for i in range(1, count + 1):
    print('\nНазвание', i, '- й страны: ', end = '')
    country = input()
    country_list += [country]
    for i in range(1, 4):
        print('Название', i, '- го города: ', end = '')
        city = input()
        city_list += [city]
print(country_list)
print(city_list)


# Решение из интернета
dataset = {}
country_num = int(input('Кол-во стран: '))
for i in range(1, country_num + 1):
    value = input('{} страна: '.format(i)).split()
    for town in value[1:]:
        dataset[town] = value[0]

print(dataset)

for i in range(1, 4):
    city = input('\n {} город: '.format(i))
    country = dataset.get(city)
    if country:
        print('Город {} расположен в стране {}'.format(city, country))
    else:
        print('По городу {} данных нет'.format(city))        



# Задача 3. Криптовалюта
data = {
    "address": "0x544444444444",
    "ETH": {
        "balance": 444,
        "totalIn": 444,
        "totalOut": 4
    },
    "count_txs": 2,
    "tokens": [
        {
            "fst_token_info": {
                "address": "0x44444",
                "name": "fdf",
                "decimals": 0,
                "symbol": "dsfdsf",
                "total_supply": "3228562189",
                "owner": "0x44444",
                "last_updated": 1519022607901,
                "issuances_count": 0,
                "holders_count": 137528,
                "price": False
            },
            "balance": 5000,
            "totalIn": 0,
            "total_out": 0
        },
        {
            "sec_token_info": {
                "address": "0x44444",
                "name": "ggg",
                "decimals": "2",
                "symbol": "fff",
                "total_supply": "250000000000",
                "owner": "0x44444",
                "last_updated": 1520452201,
                "issuances_count": 0,
                "holders_count": 20707,
                "price": False
            },
            "balance": 500,
            "totalIn": 0,
            "total_out": 0
        }
    ]
}


# 1    
print(data.items())
print([i for i in data])

# 2
data['ETH']['total_diff'] = 100
print(data)

# 3
data['tokens'][0]['fst_token_info']['name'] = 'doge'
print(data)

# 4 
x = data["tokens"][0]['total_out']
del data["tokens"][0]['total_out']
data["ETH"]['totalOut'] = x
print(data)

# 5
data['tokens'][1]['sec_token_info']['total_price'] = data['tokens'][1]['sec_token_info'].pop('price')
print(data)




# Задача 4. Товары
goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}


store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}

for name, number in goods.items():
    counts = 0
    prices = 0
    for i in store[number]:
        counts += i['quantity']
        prices += i['price'] * i['quantity']
    print('{0} - Кол-во: {1}, общая стоимость: {2}'.format(name, counts, prices))



# Задача 5. Гистограмма частоты 2
# Мой вариант:
text = input('Введите текст: ')
s = {}
m = set()
k = []
s_new = {}

for i in text:
    s[i] = text.count(i)
print(s)

# сначала выписываю значения ключей в ряд, закидываю в множество, после иду по множеству и если число
# совпадает, то закидываю ключ в список, далее присоединяю этот список к числу из множества и получаю словарь 1:[a,b,c]

for one, two in s.items():
    m.add(two)
print(m)

for one, two in s.items():
    for i in m:
        if i == two:
            k.append(one)
            s_new[i] = k

for one, two in s_new.items():
    print('{}: {}'.format(one, two))
            

# Вариант куратора
def histogram(string):
    sym_dict = dict()
    for sym in string:
        if sym in sym_dict:
            sym_dict[sym] += 1
        else:
            sym_dict[sym] = 1
    return sym_dict


def invert_dict(d):
    inv = dict()
    for key in d:
        val = d[key]
        if val not in inv:
            inv[val] = [key]
        else:
            inv[val].append(key)
    return inv


text = input('Введите текст: ')
hist = histogram(text)

print('Оригинальный словарь частот:')
for i_sym in sorted(hist.keys()):
    print(i_sym, ':', hist[i_sym])
inv_hist = invert_dict(hist)
print('\nИнвертированный словарь частот:')
for i_sym in sorted(inv_hist.keys()):
    print(i_sym, ':', inv_hist[i_sym])
    
    

# Задача 6. Словарь синонимов
count = int(input('Кол-во пар слов: '))
s = {}
for i in range(1, count + 1):
    print('\n{}-я пара слов:'.format(i))
    x = input('Введите слово: ')
    y = input('Введите синоним к этому слову: ')
    s[x] = y
    
print(s)

while True:
    word = input('Введите искомое слово: ').lower()
    if word in s:
        print('синоним к слову {}: {}'.format(word, s[x]))
        break
    else:
        print('\nТакого слова нет в словаре, попробуйте еще раз', '\n словарь: ', s)
        continue
        
        
        
# Задача 7. Пицца
# orders_count = int(input('Введите кол-во заказов: '))
# database = dict()
# for i_order in range(orders_count):
#     customer, pizza_name, count = \
#         input('{} заказ: '.format(i_order + 1)).split()
        
        
#     if customer not in database:
#         database[customer] = {pizza_name: int(count)}
#     else:
#         if pizza_name in database[customer]:
#             database[customer][pizza_name] += int(count)
#         else:
#             database[customer].update({pizza_name: int(count)})
            
            
# for i_customer in database.keys():
#     print('{}:'.format(i_customer))
#     for j_pizza in database[i_customer].keys():
#         print('     {}: {}'.format(j_pizza, database[i_customer][j_pizza]))

    
    

# Задача_8. Угадай число
# import random
# choose = int(input('Введите максимальное число: '))
# x = random.randint(1, choose)

# while True:
#     m = input('Нужное число есть среди этих чисел: ').lower()
#     if m == 'помогите!':
#         print('Артём мог загадать следующие числа:', random.randint(1, choose), x, random.randint(1, choose))
#         continue
#     m1 = [int(cash) for cash in m.split()]
    
#     if x in m1:
#         print('да')
#     else:
#         print('нет')
        
        
        
        
# Задача 9. Родословная

num = int(input('Введите количество человек: '))

tree = dict()
family = dict()

for i_index in range(num - 1):
    couple = input(f'{i_index + 1} пара: ').split()
    tree[couple[0]] = couple[1]

for child, parent in tree.items():
    if parent in tree:
        family[child] = family[parent] + 1
    else:
        family[parent] = 0
        family[child] = 1

for i_key in sorted(family):
    print(i_key, family[i_key])

    
    

# Задача_10. Слова палиндром
text = input('Введите строку: ')
m = []
plus = 0
minus = 0

for i in set(text):
    m.append(text.count(i))

for i in m:
    if i % 2 == 0:
        plus += 1
    else:
        minus += 1

if plus >= minus:
    print('Можно собрать')
elif plus < minus:
    print('нельзя собрать')
        





































































































































